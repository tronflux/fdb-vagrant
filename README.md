# README #

This is a vagrant + ansible setup for building tools on top of FoundationDB and its Python bindings.

!New in this version it now provides a dev environment for developing FoundationDB itself!

It uses my flavor of choice, Debian. (specifically Debian 9, Stretch)

It sets up a vagrant box with all the dev tools needed for developing Python applications on top of FDB as well as build tools for contributing to the FDB project itself.

### What is this repository for? ###

* Version: 0.0.1

It's got all of the things I like to have in my Python dev environment. Probably too much for your use case.
Feel free to fork and trim.

1. virtualenvwrapper
2. a premade virtualenv (named dev) with Python 3.6.5 and fdb python bindings installed. Other than that it's clean.
3. the /vagrant dir is nfs synced to the local machine with this repo as the base. Any files edited on your local machine will be reflected in the virtual environment and vice versa.
4. fdb.cluster is copied to /vagrant for connecting to the cluster on your local machine.
5. FoundationDB server and client are both isntalled, naturally.
6. Docker is installed
7. FoundationDB repo is linked as a git submodule now so that I can
8. Build the docker image
9. alias the docker run command to fdbd (remember it as FoundationDB Docker)
10. The FoundationDB repo is mounted in the Docker container under the root dir. This repo is then mounted in the Vagrant machine under /vagrant, which we already know is rsynced to this repo locally. You can use all your favorite dev tools locally and easily build FDB without the headaches of whatever the hell it is that you've done to bork up your local machine. :)

### How do I get set up? ###

* Install virtualbox
* Install vagrant
* Both of the above vary by OS. Pick your poison.

* `vagrant plugin install vagrant-vbguest`

####Note about the Docker setup and Git Submodules####
FoundationDB is linked as a git submodule. So when you clone this repo, it will *not* clone the FoundationDB repo.
You need to do two extra commands before continuing.

* `git submodule init`

* `git submodule update`

* `cd foundationdb`

* `git checkout master`

* `git branch -u origin master`

For more information about working with Git Submodules, see the [git documentation](https://git-scm.com/book/en/v2/Git-Tools-Submodules). It's really not that complicated.
####End Note####

* `vagrant plugin install vagrant-vbguest`

* cd to this dir

* `vagrant up`

* `vagrant ssh`

* `workon dev`

* `cd /vagrant`

* `python fdb_test.py`

And there you are. Working on the folder that's synced to your local machine.

It should just work, if you happen to be using something I've tested on. Which is Windows 10 Pro
and macOS High Sierra.

If you want to dive in to developing FounbationDB itself, you can launch the Docker container for building FDB.

`fdbd` will run the container in interactive mode with a shell as root. Following the instructions from the [official FoundationDB Repo](https://github.com/apple/foundationdb), we can `cd /root/foundationdb` and `make`.

Good luck and happy developing.

### Contribution guidelines ###
Don't be a dick. In case it's not clear, I have no affiliation with the FoundationDB team or with Apple. I just put this together for my own developing convenience because I like a repeatable build environment with the confort of my own local machines.